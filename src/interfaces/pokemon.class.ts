export class Pokemon {
    id: number;
    name: string;
    showPicture: boolean;
    types: Array<PokemonType>;
}

export class PokemonType {
    slot: number;
    type: TypeObject;
}

export class TypeObject {
    name: string;
}