export interface PokemonSpecies {


    /**
    *   The flavor text list of all the descriptions by region
    */
    flavor_text_entries: Array<FlavorTextEntry>;

    /**
     *   List of genera organized by available local language
     */
    genera: Array<Genus>;
}

/**
 * A pokedex description containing the text, language its in, and the game its featured by
 */
export interface FlavorTextEntry {
    flavor_text: string;
    language: FlavorTextLanguage;
    version: FlavorTextVersion;
}

/**
 * The Language entry of a FlavorTextEntry
 */
export interface FlavorTextLanguage {
    name: string;
    url: string
}

/**
 * The game version of a FlavorTextEntry
 */
export interface FlavorTextVersion {
    name: string;
    url: string
}

/**
 * The genus of a pokemon species
 */
export interface Genus {
    genus: string;
    language: any;
}

