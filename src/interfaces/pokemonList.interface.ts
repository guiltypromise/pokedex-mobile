export interface PokemonList {
    /* The total number of resources available */
    count: Number; 

    /* The URL for the next page in the list */
    next: String;

    /* The URL for the previous page in the list */
    previous: Boolean;

    /* A List of resources */
    results: Array<any>;
}