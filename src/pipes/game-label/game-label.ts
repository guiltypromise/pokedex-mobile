import { Pipe, PipeTransform } from '@angular/core';
import { TitleCasePipe } from '@angular/common';

/**
 * Generated class for the GameLabelPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'gameLabel',
})
export class GameLabelPipe implements PipeTransform {
    titlePipe: TitleCasePipe;
  /**
   * Takes a value and makes it into a Game Label.
   */
  transform(value: string, fallback: string): string {
    let textToTransform = "";  
    this.titlePipe = new TitleCasePipe();
    if( value.length > 0 ) {
        textToTransform = value.replace(/-/g,' ');// Replace all dashes with a space
        textToTransform = this.titlePipe.transform(textToTransform);
    } else {
        textToTransform = fallback;
    }
    return textToTransform;
  }
}
