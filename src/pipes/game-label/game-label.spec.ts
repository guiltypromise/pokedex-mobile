import { GameLabelPipe } from './game-label';

describe('Pipe: GameLabelPipe', () => {
    let pipe: GameLabelPipe;
  
    beforeEach(() => {
        pipe = new GameLabelPipe();
    });
   
    it('should return fallback with no value provided', () => {
        expect(pipe.transform('', 'test string')).toBe('test string');
    });

    it('should replace dashes with a space and title case it', () => {
        expect(pipe.transform('alpha-sapphire', 'alpha-sapphire')).toBe('Alpha Sapphire');
    });
  
  });