import { NgModule } from '@angular/core';
import { FeetPipe } from './feet/feet';
import { WeightPipe } from './weight/weight';
import { GameLabelPipe } from './game-label/game-label';
@NgModule({
	declarations: [FeetPipe,
    WeightPipe,
    GameLabelPipe],
	imports: [],
	exports: [FeetPipe,
    WeightPipe,
    GameLabelPipe]
})
export class PipesModule {}
