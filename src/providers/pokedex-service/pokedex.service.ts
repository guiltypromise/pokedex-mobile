import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PokemonList } from '../../interfaces/pokemonList.interface';
import { CacheService } from 'ionic-cache';
import { Pokemon } from '../../interfaces/pokemon.class';
import { environment } from '../../environments/environment';
import { PokemonSpecies } from '../../interfaces/pokemonSpecies.interface';

/*
  Generated class for the PokedexServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PokedexServiceProvider{
    knownPokemon: number = 808;

    constructor(
        public http: HttpClient,
        public cache: CacheService
    ) {
    }

    public getAllPokemon(): Observable<PokemonList> {
        let url = environment.BASE_URL + '/pokemon/?limit=808&offset=0';
        let cacheKey = url;
        let request = this.http.get<PokemonList>(url);
        return this.cache.loadFromObservable(cacheKey, request);
    }

    public getPokemonByIndex( id:Number ){
        let url = environment.BASE_URL + '/pokemon/' + JSON.stringify(id) +"/";
        let cacheKey = url;
        let request = this.http.get<Pokemon>(url);
        return this.cache.loadFromObservable(cacheKey, request);
    }

    public getPokemonBioByIndex( id:Number ) {
        let url = environment.BASE_URL + '/pokemon-species/' + JSON.stringify(id) + "/";
        let cacheKey = url;
        let request = this.http.get<PokemonSpecies>(url);
        return this.cache.loadFromObservable(cacheKey, request);
    }

}
