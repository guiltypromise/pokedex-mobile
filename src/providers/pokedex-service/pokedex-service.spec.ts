
import { TestBed, async,  } from '@angular/core/testing';

import { HttpClientTestingModule} from '@angular/common/http/testing';

import { PokedexServiceProvider} from './pokedex.service'; 
import { PokemonList } from '../../interfaces/pokemonList.interface';
import { Pokemon, PokemonType, TypeObject } from '../../interfaces/pokemon.class';
import { PokedexServiceProviderMock } from '../../../test-config/mocks-ionic';
import { PokemonSpecies, FlavorTextEntry } from '../../interfaces/pokemonSpecies.interface';


describe('Provider: Pokedex-Service', () => {
    const testTypeObj: TypeObject = {
        name:"Grass",
    }
    const testFlavorText: FlavorTextEntry = {
        flavor_text: "This is text flavor text",
        language: { name: "en", url: "www.test.com"},
        version: { name: "", url: ""},
    }
    const testSpecies: PokemonSpecies = {
        flavor_text_entries: [testFlavorText],
        genera: [],
    }
    const testTypes: PokemonType = {
        slot: 1,
        type: testTypeObj,
    }
    const testPokemon: Pokemon = {
        id: 1,
        name: "Bulbasaur",
        showPicture: false,
        types: [testTypes],
    }
    const testPokemonList: PokemonList = {
        count: 1,
        next: "",
        previous: true,
        results: [testPokemon],
    }



    beforeEach( async( () => {
        TestBed.configureTestingModule({
           imports: [HttpClientTestingModule],
           providers: [
            { provide: PokedexServiceProvider, useClass: PokedexServiceProviderMock }
           ],
        });
    }));


    // Test grab all pokemon
    it('should return a list of pokemon', () => {
        const pokeService = TestBed.get(PokedexServiceProvider);

        pokeService.getAllPokemon().subscribe((data: PokemonList) => {
            expect(data.count).toBe(1);
            expect(data).toEqual(testPokemonList);
        })

    });

    it('should return a specific pokemon', () => {
        const pokeService = TestBed.get(PokedexServiceProvider);

        pokeService.getPokemonByIndex().subscribe((data: Pokemon) => {
            expect(data).toEqual(testPokemon);
        })
    });

    it('should return a specific pokemons bio', () => {
        const pokeService = TestBed.get(PokedexServiceProvider);

        pokeService.getPokemonBioByIndex().subscribe((data: PokemonSpecies) => {
            expect(data).toEqual(testSpecies);
        })
    });
    

});