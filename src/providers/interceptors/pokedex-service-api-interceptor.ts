import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { environment } from "../../environments/environment";



@Injectable()
export class PokedexServiceApiInterceptor implements HttpInterceptor {

    public constructor() {
        console.log('Hello PokedexServiceProvider Provider');
    }

    intercept(req: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {

        // add special header attributes on every request
        // if( req.url.includes(environment.BASE_URL)) {
        //     const clone = req.clone({ setHeaders:{
        //         'withCredentials': 'true',
        //         'Content-Type': 'application/json; charset=utf-8',
        //         'Accept': '/',
        //         'Upgrade-Insecure-Requests': '1',
        //         'Access-Control-Allow-Credentials': "true",
        //         'Access-Control-Allow-Origin': "*",
        //         'Access-Control-Allow-Methods': "GET, POST, PUT, DELETE, OPTIONS",
        //         'Access-Control-Allow-Headers': "Content-Type,Authorization,Upgrade-Insecure-Requests"

        //     }});
        //     return next.handle(clone);
        // }

        return next.handle(req);
    }
}