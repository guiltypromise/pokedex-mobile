import { async, TestBed } from '@angular/core/testing';
import { NavController, ViewController, NavParams, IonicModule, AlertController } from 'ionic-angular';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

import { PokemonDetailsModalPage } from './pokemon-details-modal';
import {
  NavParamsMock,
  ViewControllerMock,
  PokedexServiceProviderMock,
  NavMock
} from '../../../test-config/mocks-ionic';
import { PokedexServiceProvider } from '../../providers/pokedex-service/pokedex.service';


describe('Page: PokemonDetailsModalPage', () => {
  let fixture;
  let component;
  let mockPokeService;

  beforeEach(async(() => {
      mockPokeService = new PokedexServiceProviderMock();
    TestBed.configureTestingModule({
      declarations: [PokemonDetailsModalPage],
      imports: [
        ComponentsModule,
        PipesModule,
        IonicModule.forRoot(PokemonDetailsModalPage),
      ],
      providers: [
        { provide: PokedexServiceProvider, useValue: mockPokeService },
        { provide: NavParams, useClass: NavParamsMock },
        { provide: ViewController, useClass: ViewControllerMock },
        { provide: NavController, useClass: NavMock },
        AlertController,
      ]
    })
  }));

  beforeEach(() => {
      // run setup
    fixture = TestBed.createComponent(PokemonDetailsModalPage);
    component = fixture.componentInstance;
  });

  afterEach( () => {
      // run cleanup code
  });

  it('should be created', () => {
    expect(component).toBeDefined();
  });

  it('should have flavor text entries', () => {
    spyOn(component, "initPokemonFlavorText");
    component.initPokemonFlavorText();
    expect(component.initPokemonFlavorText).toHaveBeenCalled();
  });



});