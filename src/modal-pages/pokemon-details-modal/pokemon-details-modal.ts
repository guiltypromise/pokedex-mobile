import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { PokedexServiceProvider } from '../../providers/pokedex-service/pokedex.service';
import { Pokemon } from '../../interfaces/pokemon.class';
import { PokemonSpecies, Genus, FlavorTextEntry } from '../../interfaces/pokemonSpecies.interface';
import { GameLabelPipe } from '../../pipes/game-label/game-label';


/**
 * Generated class for the PokemonDetailsModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pokemon-details-modal',
  templateUrl: 'pokemon-details-modal.html',
})
export class PokemonDetailsModalPage {

    id: number;
    pokemon: Pokemon;
    pokemonBio: PokemonSpecies;
    genus: Genus;
    flavorText: Array<FlavorTextEntry>;
    isLoading: boolean;
    viewMode: string;
    locale: string;
    type: string;
    type2: string = "";
    type2class: string = "";
    flavorTextRadioOptions = [];
    currentFlavorText: string;
    currentGameSource: string;

    gameLabelPipe: GameLabelPipe;

    constructor(  public navCtrl: NavController
                , public navParams: NavParams
                , public viewCtrl: ViewController
                , public alertCtrl: AlertController
                , public pokeService: PokedexServiceProvider
    ) {
    }

    ngOnInit() {
        this.gameLabelPipe = new GameLabelPipe();
        this.locale = "en";
        this.viewMode = 'bio';
        this.isLoading = true;
        this.id = this.navParams.get('id');
        console.log('ionViewDidLoad PokemonDetailsModalPage');
        this.initPokemonData();
    }

    ionViewWillEnter() {
        if( this.id === undefined) {
            this.navCtrl.setRoot('BrowsePage');
        }
    }

    /**
     * Initializes data for this pokemon
     */
    initPokemonData() {
        this.pokeService.getPokemonByIndex( this.id ).subscribe(
            data => {
                this.pokemon = data;
                this.pokemon.types.forEach( typeObj => {
                    console.log(typeObj);
                    if( typeObj.slot === 1) {
                        this.type = typeObj.type.name;
                    }
                    if( typeObj.slot === 2) {
                        this.type2class = "-" + typeObj.type.name;
                        this.type2 = typeObj.type.name;
                    } 
                });

                this.initPokemonBio();

            }
        );
    }

    /**
     * Initializes the bio info of this pokemon
     */
    initPokemonBio() {
        this.pokeService.getPokemonBioByIndex( this.id ).subscribe(
            data => {
                this.pokemonBio = data;
                this.initPokemonGenus();
                this.initPokemonFlavorText();
                this.isLoading = false;

            }
        );
    }

    initPokemonGenus() {
        this.pokemonBio.genera.forEach( genus => {
            if( genus.language['name'] === this.locale ) {
                this.genus = genus;
            }
        });
    }

    initPokemonFlavorText() {
        this.flavorText = [];
        this.pokemonBio.flavor_text_entries.forEach( entry => {
            if( entry.language.name === this.locale) {
                console.log(entry);
                this.flavorText.push(entry);
                let radioOption = {
                    type: 'radio',
                    label: this.gameLabelPipe.transform(entry.version.name, entry.version.name),
                    value: entry,
                }
                this.flavorTextRadioOptions.push( radioOption );
            }
        });
        this.currentFlavorText = this.flavorText[this.flavorText.length - 1].flavor_text;
        this.currentGameSource = this.flavorText[this.flavorText.length - 1].version.name;
    }

    showFlavorTextRadioOptions() {
        let alert = this.alertCtrl.create();
        alert.setTitle('Set Game Source');
        for( var i = this.flavorTextRadioOptions.length-1; i > 0; i--) {
            let option = this.flavorTextRadioOptions[i];
            if( option['label'] === this.gameLabelPipe.transform(this.currentGameSource, this.currentGameSource)) {
                option['checked'] = true;
            } else {
                option['checked'] = false;
            }
            option['handler'] = (data) => {
                this.currentGameSource = data.value.version.name;
                this.currentFlavorText = data.value.flavor_text;
                alert.dismiss();
                return false;
            }
            alert.addInput( option );
        };

        alert.addButton({
            text: 'Cancel',
            handler: () => {
                alert.dismiss();
                return false;
            }
        });

        alert.present();
    }

    backButtonClick(){
        this.navCtrl.pop();
    }
}
