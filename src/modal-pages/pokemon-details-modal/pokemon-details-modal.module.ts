import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PokemonDetailsModalPage } from './pokemon-details-modal';
import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    PokemonDetailsModalPage,
  ],
  imports: [
      ComponentsModule,
      PipesModule,
    IonicPageModule.forChild(PokemonDetailsModalPage),
  ],
})
export class PokemonDetailsModalPageModule {}
