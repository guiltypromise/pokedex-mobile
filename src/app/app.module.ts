import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CacheModule } from 'ionic-cache';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import {ComponentsModule} from '../components/components.module';
import { PokedexServiceProvider } from '../providers/pokedex-service/pokedex.service';
import { IonicStorageModule } from '@ionic/storage';
import { PokedexServiceApiInterceptor } from '../providers/interceptors/pokedex-service-api-interceptor';

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    ComponentsModule,
    CacheModule,
    HttpClientModule,
    CacheModule.forRoot({ keyPrefix: 'app-cache'}),
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    PokedexServiceProvider,
    InAppBrowser,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide: HTTP_INTERCEPTORS, useClass: PokedexServiceApiInterceptor, multi:true},
  ]
})
export class AppModule {}
