import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { CacheService } from 'ionic-cache';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'MainMenuPage';

  pages: Array<{title: string, component: any}>;

  constructor(
        public platform: Platform, 
        public statusBar: StatusBar, 
        public splashScreen: SplashScreen,
        public cache: CacheService
    ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Main Menu', component: 'MainMenuPage' },
      { title: 'Browse', component: 'BrowsePage' },
      //{ title: 'List', component: 'ListPage' },
      { title: 'Guess That Pkmn', component: 'GuessingGamePage' },
      //{ title: 'Pokedex', component: 'PokedexPage'}
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
        this.cache.setDefaultTTL(60 * 60);
        this.cache.setOfflineInvalidate(false);
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        //this.statusBar.styleDefault();
        //this.statusBar.backgroundColorByHexString('#FF4040');
        if( this.platform.is('android') || this.platform.is('cordova') ){
            this.statusBar.overlaysWebView(true);
            this.statusBar.styleBlackTranslucent();
            this.splashScreen.hide();
        }

    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
