import { Component } from '@angular/core';

/**
 * Generated class for the ModalCustomComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'modal-custom',
  templateUrl: 'modal-custom.html'
})
export class ModalCustomComponent {

  text: string;

  constructor() {
    console.log('Hello ModalCustomComponent Component');
    this.text = 'Hello World';
  }

}
