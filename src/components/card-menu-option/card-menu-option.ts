import { Component, Input } from '@angular/core';

/**
 * Generated class for the CardMenuOptionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'card-menu-option',
  templateUrl: 'card-menu-option.html'
})
export class CardMenuOptionComponent {

  text: string;
  @Input() header: string;

  constructor() {
    console.log('Hello CardMenuOptionComponent Component');
    this.text = 'Hello World';
  }

}
