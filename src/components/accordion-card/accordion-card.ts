import { Component, Input } from '@angular/core';

/**
 * Generated class for the AccordionCardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'accordion-card',
  templateUrl: 'accordion-card.html'
})
export class AccordionCardComponent {

  @Input('header') headerText?:string;

  constructor() {
    console.log('Hello AccordionCardComponent Component');
  }

}
