import { async, TestBed } from '@angular/core/testing';
import { IonicModule } from 'ionic-angular';
import { AccordionCardComponent } from './accordion-card';


let suiteDesc = 'Component: Accordion-Card';

describe(suiteDesc, () => {
  let fixture;
  let component;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AccordionCardComponent],
      imports: [
        IonicModule.forRoot(AccordionCardComponent)
      ],
    })
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccordionCardComponent);
    component = fixture.componentInstance;
  });

  it('should be created', () => {
    expect(component instanceof AccordionCardComponent).toBe(true);
  });

});