import { NgModule } from '@angular/core';
import { AccordionCardComponent } from './accordion-card/accordion-card';
import { IonicModule } from 'ionic-angular';
import { CommonModule } from '@angular/common';
import { ModalCustomComponent } from './modal-custom/modal-custom';
import { CardMenuOptionComponent } from './card-menu-option/card-menu-option';
import { CardMenuOptionItemComponent } from './card-menu-option-item/card-menu-option-item';
import { BrowseFilterPopoverComponent } from './browse-filter-popover/browse-filter-popover';
import { ExternalLinkComponent } from './external-link/external-link';


@NgModule({
    declarations: [
        AccordionCardComponent,
    ModalCustomComponent,
    CardMenuOptionComponent,
    CardMenuOptionItemComponent,
    BrowseFilterPopoverComponent,
    ExternalLinkComponent,
    ],
    entryComponents: [
        BrowseFilterPopoverComponent,

    ],
    imports: [
        CommonModule,
        IonicModule,
    ],
    exports: [
        AccordionCardComponent,
    ModalCustomComponent,
    CardMenuOptionComponent,
    CardMenuOptionItemComponent,
    BrowseFilterPopoverComponent,
    ExternalLinkComponent,
    ]
})
export class ComponentsModule {}
