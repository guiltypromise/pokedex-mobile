import { TestBed } from "@angular/core/testing";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { ExternalLinkComponent } from "./external-link";
import { DebugElement } from "@angular/core";
import { By } from "@angular/platform-browser";


describe('Component: External Link', () => {
    let fixture;
    let component;
    let linkElement: DebugElement;

    beforeEach( async() => {
        TestBed.configureTestingModule({
            declarations: [ExternalLinkComponent],
            providers: [InAppBrowser],
        })
    })

    beforeEach( () => {
        fixture = TestBed.createComponent(ExternalLinkComponent);
        component = fixture.componentInstance;
        linkElement = fixture.debugElement.query(By.css('span'));
    })

    it('should be created', () => {
        expect(component instanceof ExternalLinkComponent).toBe(true);
    });

});