import {Component, Input} from '@angular/core';
import {InAppBrowser, InAppBrowserOptions} from "@ionic-native/in-app-browser";

/**
 * Generated class for the ExternalLinkComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'external-link',
  templateUrl: 'external-link.html'
})
export class ExternalLinkComponent {
  @Input('url') url;
  @Input('title') title;
  @Input('idDescription') idDescription;
  text: string;
  idDesc: string;

  constructor(private iab: InAppBrowser) {
    console.log('Hello ExternalLinkComponent Component');
    this.text = this.title;
    this.idDesc = this.idDescription;

  }
 openBrowser(){
    const options:InAppBrowserOptions = {
        hardwareback: 'yes',
    }
   const browser = this.iab.create(this.url, '_system', options);
 }
}
