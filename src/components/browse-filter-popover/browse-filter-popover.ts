import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

/**
 * Generated class for the BrowseFilterPopoverComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'browse-filter-popover',
  templateUrl: 'browse-filter-popover.html'
})
export class BrowseFilterPopoverComponent {

  text: string;

  constructor( public viewCtrl: ViewController) {
    console.log('Hello BrowseFilterPopoverComponent Component');
    this.text = 'Hello World';
  }

  close( startIndex ) {
    this.viewCtrl.dismiss( startIndex );
  }

}
