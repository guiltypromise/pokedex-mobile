import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';

/**
 * Generated class for the CardMenuOptionItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'card-menu-option-item',
  templateUrl: 'card-menu-option-item.html'
})
export class CardMenuOptionItemComponent {

  text: string;
  @Input() header: string;
  @Input() navTarget: string;
  @Input() imgSrcPath: string;


  constructor( public navCtrl: NavController) {
    console.log('Hello CardMenuOptionItemComponent Component');
    this.text = 'Hello World';
  }

  goToPage(){
    this.navCtrl.setRoot(this.navTarget);
  }

}
