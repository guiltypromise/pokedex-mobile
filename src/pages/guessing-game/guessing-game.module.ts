import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GuessingGamePage } from './guessing-game';

@NgModule({
  declarations: [
    GuessingGamePage,
  ],
  imports: [
    IonicPageModule.forChild(GuessingGamePage),
  ],
})
export class GuessingGamePageModule {}
