import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PokedexServiceProvider } from '../../providers/pokedex-service/pokedex.service';
import { Pokemon } from '../../interfaces/pokemon.class';

/**
 * Generated class for the GuessingGamePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-guessing-game',
  templateUrl: 'guessing-game.html',
})
export class GuessingGamePage {

    pokemonList;

    pokemon = undefined;

    isLoading = false;

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public pokeService:   PokedexServiceProvider,
        ) {
    }

    ionViewWillEnter() {
        //this.initPokemonData();
        this.pokemon = new Pokemon();
        this.pokemon.showPicture = false;
        this.getRandomPokemon();
    }

    ionViewWillLeave() {        

    }

    initPokemonData() {
        this.pokeService.getAllPokemon().subscribe(
            data => {
                this.pokemonList = data.results;
            }
        );
    }

    getRandomPokemon() {
        this.isLoading = true;
        this.pokeService.getPokemonByIndex( Math.round(Math.random() * 801) + 1).subscribe(
            data=> {
                this.isLoading = false;
                this.pokemon = data;
                this.pokemon.showPicture = false;
            }
        );
    }

    openDetailsModal(){
        this.navCtrl.push('PokemonDetailsModalPage', {id: this.pokemon.id})
    }

    refreshPokemonData() {
        this.getRandomPokemon();
    }

    revealPicture( item ) {
        item.showPicture = true;
    }

}
