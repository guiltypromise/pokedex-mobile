import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PokedexPage } from './pokedex';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    PokedexPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(PokedexPage),
  ]
})
export class PokedexPageModule {}
