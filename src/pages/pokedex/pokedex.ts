import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

/**
 * Generated class for the PokedexPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pokedex',
  templateUrl: 'pokedex.html'
})
export class PokedexPage {

  alphaRoot = 'AlphaPage'
  numericRoot = 'NumericPage'


  constructor(public navCtrl: NavController) {}

}
