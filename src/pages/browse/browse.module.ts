import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BrowsePage } from './browse';
import { ComponentsModule } from '../../components/components.module';
import { DirectivesModule } from '../../directives/directives.module';
@NgModule({
  declarations: [
      BrowsePage
  ],
  entryComponents: [
    
  ],
  imports: [ 
      DirectivesModule,
      ComponentsModule,
      IonicPageModule.forChild(BrowsePage)
  ],
})
export class BrowseModule {}