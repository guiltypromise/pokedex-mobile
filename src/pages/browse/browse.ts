import { Component, ViewChild } from '@angular/core';
import { NavController, LoadingController, IonicPage, ModalController, Content, Loading, PopoverController } from 'ionic-angular';
import { PokedexServiceProvider } from '../../providers/pokedex-service/pokedex.service';
import { CacheService } from 'ionic-cache';
import { ScrollHideConfig } from '../../directives/scroll-hide/scroll-hide';
import { BrowseFilterPopoverComponent } from '../../components/browse-filter-popover/browse-filter-popover';


@IonicPage()
@Component({
  selector: 'page-browse',
  templateUrl: 'browse.html'
})
export class BrowsePage {

    @ViewChild(Content) content: Content;

    headerScrollConfig: ScrollHideConfig = { cssProperty: 'margin-top', maxValue: 44 };

    pokemonList: Array<any>;
    filteredPokemonList: Array<any>
    knownPokemon: number = 808;
    selectedId: number;
    searchQuery: string;
    isLoading: boolean;
    loadSpinner: Loading;

    constructor(
        public navCtrl:       NavController,
        public loader:        LoadingController,
        public pokeService:   PokedexServiceProvider,
        public cacheService:  CacheService,
        public modalCtrl:     ModalController,
        public loadCtrl:      LoadingController,
        public popoverCtrl:   PopoverController,
    ) {
    
    }

    ionViewDidLoad() {
        this.loadSpinner = this.loadCtrl.create({
            spinner: 'crescent',
            content: 'Retrieving Pokemon data ...',
        });
        this.isLoading = true;
        this.initPokemonData();
    }

    ionViewDidEnter() {
        if( this.searchQuery !== undefined ) {
            this.filterPokemonList();
        }
    }

    ionViewWillLeave() {

    }

    initPokemonData() {
        this.loadSpinner.present();
        this.pokeService.getAllPokemon().subscribe(
            data => {
                this.pokemonList = data.results;
                this.filteredPokemonList = data.results;
                this.initIndices();
                this.isLoading = false;
                this.loadSpinner.dismiss();
            }
        );
    }

    initIndices() {
        for( let i = 0 ; i<(this.knownPokemon); i++){
            let filteredPokemon = this.filteredPokemonList[i];
            let pokemon = this.pokemonList[i];
            if( filteredPokemon !== undefined && pokemon !== undefined) {
                filteredPokemon['id'] = i+1;
                pokemon['id'] = i+1;
            } else {
                console.error("Have undefined pokemon at index: " + i);
            }

        }
    }

    filterPokemonList() {
        this.filteredPokemonList = this.pokemonList.filter( e => e.name.toUpperCase().search(this.searchQuery.toUpperCase()) !== -1 || Number.parseInt(this.searchQuery) === e.id);
    }

    openDetailsModal(pokemon){
        this.selectedId = pokemon.id;
        this.pokeService.getPokemonByIndex( pokemon.id ).toPromise()
        .then(  () => {
            this.navCtrl.push('PokemonDetailsModalPage', {id: pokemon.id,});
        });
    }

    openFilterPopover( myEvent ) {
 
        let popover = this.popoverCtrl.create(BrowseFilterPopoverComponent);
        popover.present({
            ev: myEvent
        });

        popover.onWillDismiss( index => {
            if( index ) {
                this.scrollTo(index);

            }
        });
          
    }

    scrollTo( id: string) {
        let y = document.getElementById("item_" + id).offsetTop;
        this.content.scrollTo(0, y);
    }

}
