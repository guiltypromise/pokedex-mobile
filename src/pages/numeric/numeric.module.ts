import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NumericPage } from './numeric';

@NgModule({
  declarations: [
    NumericPage,
  ],
  imports: [
    IonicPageModule.forChild(NumericPage),
  ],
})
export class NumericPageModule {}
