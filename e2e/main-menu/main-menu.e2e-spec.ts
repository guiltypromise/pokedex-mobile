import { MainMenuPage } from './main-menu.po';
import { browser, by, element } from 'protractor';

describe('--- Main Menu Page ---', () => {
  let page: MainMenuPage;

  beforeEach(() => {
    page = new MainMenuPage();
  });

  describe('Initial Main Menu page load', () => {
    beforeEach(() => {
      page.navigateTo('/');
    });

    it('should have a title saying Main Menu', () => {
      page.getTitleText().then(title => {
        expect(title).toEqual('Main Menu');
      });
    });

    it('should have a nav card item for the Browse Page', () => {
        page.getBrowseNavCardItem().then( header => {
            expect(header).toEqual('Pokédex');
        })
    });

    it('should have a nav card item for the Guessing Game Page', () => {
        page.getGuessingGameNavCardItem().then( header => {
            expect(header).toEqual("Who's That Pokémon?");
        })
    });
  });

  describe('Navigate from Main Menu to Browse', () => {
      beforeEach(() => {
          page.navigateTo('/#/');
      });

      it('should tap on Pokedex nav card and navigate to the Browse page', () => {
        page.goToBrowsePage().then( () => {
            expect(browser.getCurrentUrl()).toContain('browse');
        });
      });
  })

  describe('Navigate from Main Menu to Guessing Game', () => {
    beforeEach(() => {
        page.navigateTo('/#/');
    });

    it('should tap on Guessing Game nav card and navigate to the Guessing Game page', () => {
      page.goToGuessingGamePage().then( () => {
          expect(browser.getCurrentUrl()).toContain('guessing-game');
      });
    });
})


  
});