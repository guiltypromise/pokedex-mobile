import { browser, by, element } from 'protractor';

export class MainMenuPage {

  navigateTo(destination) {
    return browser.get(destination, 10000);
  }

  getTitle() {
    return browser.getTitle();
  }

  getTitleText() {
    return element(by.tagName('page-main-menu'))
          .element(by.id('title_MainMenu'))
          .element(by.css('.toolbar-title'))
          .getText();
  }

  getBrowseNavCardItem() {
      return element(by.tagName('page-main-menu'))
            .element(by.id('button_navToBrowsePage'))
            .element(by.tagName('ion-card-header'))
            .getText();
  }

  getGuessingGameNavCardItem() {
    return element(by.tagName('page-main-menu'))
    .element(by.id('button_navToGuessingGamePage'))
    .element(by.tagName('ion-card-header'))
    .getText();
  }

  goToBrowsePage() {
      return element(by.tagName('page-main-menu'))
            .element(by.id('button_navToBrowsePage')).click();
  }

  goToGuessingGamePage() {
    return element(by.tagName('page-main-menu'))
          .element(by.id('button_navToGuessingGamePage')).click();
  }   
}
