import { browser, by, element } from 'protractor';

export class BrowsePage {

  navigateTo(destination) {
    return browser.get(destination, 10000);
  }

  getTitle() {
    return browser.getTitle();
  }

  getTitleText() {
    return element(by.tagName('page-browse'))
          .element(by.id('title_Browse'))
          .element(by.css('.toolbar-title'))
          .getText();
  }

  getMoreOptionsButton() {
      return element(by.tagName('page-browse'))
            .element(by.id('button_BrowseMoreOptions')).isPresent();
  }
}
