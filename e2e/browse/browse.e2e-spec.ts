import { BrowsePage } from './browse.po';

describe('--- Browse Page ---', () => {
  let page: BrowsePage;

  beforeEach(() => {
    page = new BrowsePage();
  });

  describe('Initial Browse page load', () => {
    beforeEach(() => {
      page.navigateTo('/#/browse');
    });

    it('should have a title saying Browse', () => {
      page.getTitleText().then(title => {
        expect(title).toEqual('Browse');
      });
    });

    it('should have a More Options button', () => {
        page.getMoreOptionsButton().then( (element) => {
            expect(element).toBeDefined();
        });
    })
  })


  
});